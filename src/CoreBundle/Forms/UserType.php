<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16-Jan-19
 * Time: 15:54
 */

namespace CoreBundle\Forms;


use CoreBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function getBlockPrefix()
    {
        return 'user_type';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options); // TIME TO WOLOLO

        $builder->add('username', TextType::class, array(
            'label' => 'Username',
            'required' => true
        ));

        $builder->add('email', EmailType::class, array(
            'label' => 'Email',
            'required' => true
        ));

        $builder->add('active', CheckboxType::class, array(
            'label' => 'Active',
            'required' => false
        ));

        $builder->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'first_options' => ['label' => 'Password'],
            'second_options' => ['label' => 'Repeat password'],
            'required' => true,
            'invalid_message' => 'Les champs ne sont égaux'
        ));

        $builder->add('save', SubmitType::class, [
            'attr' => ['class' => 'btn btn-success pull-right'],
            'label' => 'Save user'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver); // TIME TO WOLOLO

        $resolver->setDefault('data_class', User::class);
    }
}