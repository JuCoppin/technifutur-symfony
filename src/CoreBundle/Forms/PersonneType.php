<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 15-Jan-19
 * Time: 11:57
 */

namespace CoreBundle\Forms;


use CoreBundle\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonneType extends AbstractType
{
    public function getBlockPrefix()
    {
        return 'personne_type';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, array(
            'attr' => array('placeholder' => 'Quel est votre email ?'),
            'label' => 'Email',
            'required' => true,
        ));

        $builder->add('firstname', TextType::class, array(
            'attr' => array('autofocus' => 'true'),
            'label' => 'Firstname',
            'required' => true,
        ));
        
        $builder->add('save', SubmitType::class, [
            'label' => 'UN LABEL'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver); // TIME TO WOLOLO

        $resolver->setDefault('data_class', Personne::class);
    }
}