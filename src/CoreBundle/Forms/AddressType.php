<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16-Jan-19
 * Time: 10:45
 */

namespace CoreBundle\Forms;


use CoreBundle\Entity\Address;
use CoreBundle\Entity\Personne;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options); // TIME TO WOLOLO

        $builder->add('line1', TextType::class, array(
            'label' => 'Line 1',
            'required' => true
        ));

        $builder->add('zipCode', TextType::class, array(
            'label' => 'Zip Code',
            'required' => true
        ));

        $builder->add('personne', EntityType::class, array(
            'class' => Personne::class,
            'label' => 'Propriétaire',
            'required' => true
        ));

        $builder->add('save', SubmitType::class, array(
            'attr' => ['class' => 'btn btn-success btn-lg pull-right']
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver); // TIME TO WOLOLO

        $resolver->setDefault('data_class', Address::class);
    }

    public function getBlockPrefix()
    {
        return 'address_type';
    }

}