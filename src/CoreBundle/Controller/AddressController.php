<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16-Jan-19
 * Time: 10:42
 */

namespace CoreBundle\Controller;


use CoreBundle\Entity\Address;
use CoreBundle\Forms\AddressType;
use Symfony\Component\HttpFoundation\Request;

class AddressController extends MasterController
{
    public function createAction(Request $request)
    {
        $address = new Address();

        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();
        }

        return $this->render('@Core/Address/create.html.twig', [
            'address' => $address, 'form' => $form->createView()
        ]);
    }
}