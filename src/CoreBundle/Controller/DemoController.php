<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 08-Jan-19
 * Time: 13:40
 */

namespace CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends MasterController
{
    public function indexAction()
    {
        return $this->render('@Core/Demo/index.html.twig');
    }
}