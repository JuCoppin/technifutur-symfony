<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 15-Jan-19
 * Time: 14:34
 */

namespace CoreBundle\Controller;


use CoreBundle\Entity\Personne;
use CoreBundle\Forms\PersonneType;
use Symfony\Component\HttpFoundation\Request;

class PersonneController extends MasterController
{
    public function registerPersonneAction(Request $request)
    {
        dump($_POST);
        $p = new Personne();
        $p->setLastname('BOUH');
//        dump($p);
        $form = $this->createForm(PersonneType::class, $p);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($p);
            $em->flush();
        }
//        dump($p);
        return $this->render('@Core/Personne/register.html.twig', [
            'registerForm' => $form->createView(), 'p' => $p
        ]);
    }
}